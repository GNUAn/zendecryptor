#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "endecryptor.c"
/*
 * Copyright 2024 AnatoliyL
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/*
*   Client for zencryptor and zdecryptor.
*/
int main(int argc, char **argv)
{
  if (argc < 4 || strcmp(argv[1], "-e") != 0 || strcmp(argv[1], "-d") != 0 || strcmp(argv[1], "--help") == 0)
    {
      printf("Usage: %s [-d|-e] <message> <magic number>\n", argv[0]);
      printf("Usage: %s --help\n", argv[0]);
      return -1;
    }
  else if (strcmp(argv[1], "-d") == 0 && argc >= 4)
    {
      printf("%s\n", decrypt(argv[2], atoi(argv[3])));
    }
  else if (strcmp(argv[1], "-e") == 0 && argc >= 4)
    {
      printf("%s\n", encrypt(argv[2], atoi(argv[3])));
    }

  return 0;
}
